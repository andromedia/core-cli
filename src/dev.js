"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.dev = void 0;
const child_process_1 = require("child_process");
const fs_1 = __importDefault(require("fs"));
const path_1 = require("path");
const ask_app_data_1 = require("./dev/ask-app-data");
const clone_base_app_1 = require("./dev/clone-base-app");
const clone_core_libs_1 = require("./dev/clone-core-libs");
const prepare_git_1 = require("./dev/prepare-git");
const root = path_1.join(process.cwd(), "..", "..");
exports.dev = () => __awaiter(void 0, void 0, void 0, function* () {
    if (!isProjectReady()) {
        console.log("\n\nFresh project detected. \nCreating new Project:\n");
        const data = yield ask_app_data_1.askAppData(root);
        if (data) {
            yield prepare_git_1.prepareGit(data, root);
            yield clone_core_libs_1.cloneCoreLibs(data, root);
            yield clone_base_app_1.cloneBaseApp(data, root);
            yield run("yarn");
        }
    }
});
const isProjectReady = () => {
    return fs_1.default.existsSync(path_1.join(root, "app"));
};
const run = (command, silent = false) => {
    return new Promise((resolve) => {
        const cwd = path_1.join(root);
        const str = command.split(" ");
        const cmd = str.shift();
        if (cmd) {
            const child = child_process_1.spawn(cmd, [...str], {
                stdio: silent ? "ignore" : "inherit",
                cwd,
            });
            child.on("close", () => resolve());
            child.on("exit", () => resolve());
        }
    });
};
