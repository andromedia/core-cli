export interface AppData {
    name: string;
    pkgname: string;
    namespace: string;
    git: string;
    isCoreDev: boolean;
}
export declare const askAppData: (root: string) => Promise<AppData | undefined>;
