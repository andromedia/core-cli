"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.askAppData = void 0;
const fs_1 = require("fs");
const inquirer_1 = __importDefault(require("inquirer"));
const path_1 = require("path");
exports.askAppData = (root) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = yield inquirer_1.default.prompt([
            {
                type: "input",
                name: "name",
                message: "App Name: ",
            },
        ]);
        const pkgname = toSnakeCase(data.name.trim());
        let namespace = {
            namespace: "",
        };
        while (namespace.namespace.indexOf(".") < 0) {
            namespace = yield inquirer_1.default.prompt([
                {
                    type: "input",
                    name: "namespace",
                    message: "App Namespace: ",
                    default: "com.andromedia." + pkgname,
                },
            ]);
            if (namespace.namespace.replace(/\s/g, "").indexOf(".") < 0) {
                console.log("ERROR: namespace should have . (dot) character");
            }
        }
        const git = yield inquirer_1.default.prompt([
            {
                type: "input",
                name: "url",
                message: "Git URL (enter to skip): ",
            },
        ]);
        data.name = pkgname;
        data.namespace = namespace.namespace.replace(/\s/g, "");
        data.git = git.url;
        return {
            name: data.name,
            pkgname: pkgname,
            namespace: data.namespace,
            git: data.git,
            isCoreDev: fs_1.existsSync(path_1.join(root, "libs", "cli", "src")),
        };
    }
    catch (e) {
        console.log("Stopped when asking app detail...");
    }
});
const toSnakeCase = (str) => {
    return str
        .replace(/\W+/g, " ")
        .split(/ |\B(?=[A-Z])/)
        .map((word) => word.toLowerCase())
        .join("_");
};
