import { AppData } from "./ask-app-data";
export declare const cloneBaseApp: (data: AppData, root: string) => Promise<void>;
