"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.cloneBase = void 0;
const fs_1 = __importDefault(require("fs"));
const isomorphic_git_1 = __importDefault(require("isomorphic-git"));
const node_1 = __importDefault(require("isomorphic-git/http/node"));
const path_1 = require("path");
const loading_1 = require("../utils/loading");
const rimraf_promise_1 = __importDefault(require("rimraf-promise"));
exports.cloneBase = (data, root) => __awaiter(void 0, void 0, void 0, function* () {
    loading_1.loading.start("Cloning BaseApp");
    yield isomorphic_git_1.default.clone({
        fs: fs_1.default,
        http: node_1.default,
        dir: path_1.join(root, "app"),
        url: "https://bitbucket.org/andromedia/core-base",
        ref: "master",
        singleBranch: true,
        depth: 1,
    });
    addCoreToWorkspace(data, root);
    yield rimraf_promise_1.default(path_1.join(root, "app", ".git"));
    loading_1.loading.info("BaseApp cloned");
});
const addCoreToWorkspace = (data, root) => {
    const pkg = JSON.parse(fs_1.default.readFileSync(path_1.join(root, "package.json"), "utf-8"));
    if (!pkg.workspaces) {
        pkg.workspaces = [];
    }
    if (pkg.workspaces.indexOf("app") < 0) {
        pkg.workspaces.push("app");
    }
    if (!pkg.dependencies) {
        pkg.dependencies = {};
    }
    if (pkg.dependencies) {
        pkg.dependencies = Object.assign(Object.assign({}, pkg.dependencies), { app: "workspace:app" });
    }
    fs_1.default.writeFileSync(path_1.join(root, "package.json"), JSON.stringify(pkg, null, 2));
};
