"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.prepareGit = void 0;
const fs_1 = __importStar(require("fs"));
const isomorphic_git_1 = __importDefault(require("isomorphic-git"));
const node_1 = __importDefault(require("isomorphic-git/http/node"));
const path_1 = require("path");
const rimraf_promise_1 = __importDefault(require("rimraf-promise"));
const loading_1 = require("../utils/loading");
exports.prepareGit = (data, root) => __awaiter(void 0, void 0, void 0, function* () {
    if (data.git) {
        loading_1.loading.start("Cloning: " + data.git);
        // clone new git
        yield isomorphic_git_1.default.clone({
            fs: fs_1.default,
            http: node_1.default,
            dir: path_1.join(root, "new_git"),
            url: data.git,
            ref: "master",
            singleBranch: true,
        });
        if (fs_1.existsSync(path_1.join(root, ".git"))) {
            yield rimraf_promise_1.default(path_1.join(root, ".git"));
        }
        fs_1.default.renameSync(path_1.join(root, "new_git", ".git"), path_1.join(root, ".git"));
        yield rimraf_promise_1.default(path_1.join(root, "new_git"));
        loading_1.loading.info("Git successfully cloned");
    }
    //   replaceFileBetween(
    //     join(root, ".gitignore"),
    //     "# yarn berry: start #",
    //     "# yarn berry: end #",
    //     `
    // .yarn/*
    // !.yarn/releases
    // !.yarn/plugins
    // !.yarn/sdks
    // !.yarn/versions
    // .pnp.*
    // `
    //   );
});
