export declare const gitCredential: () => Promise<{
    username: string;
    password: string;
}>;
