"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.gitCredential = void 0;
const path_1 = require("path");
const fs_1 = __importDefault(require("fs"));
const inquirer_1 = __importDefault(require("inquirer"));
const cryptr_1 = __importDefault(require("cryptr"));
const cryptr = new cryptr_1.default("Andromedia-Secret-Key-12345-Okedeh-Haloha");
exports.gitCredential = () => __awaiter(void 0, void 0, void 0, function* () {
    const rootDir = path_1.join(process.cwd(), "..", "..");
    const credFile = path_1.join(rootDir, "cli.credential");
    if (!fs_1.default.existsSync(credFile)) {
        console.log("\nBitbucket Login: ");
        const data = yield inquirer_1.default.prompt([
            {
                type: "input",
                name: "username",
                message: "Username: ",
            },
            {
                type: "password",
                name: "password",
                message: "Password: ",
            },
        ]);
        const content = cryptr.encrypt(JSON.stringify(data));
        fs_1.default.writeFileSync(credFile, content);
        return data;
    }
    const content = fs_1.default.readFileSync(credFile);
    return JSON.parse(cryptr.decrypt(content.toString()));
});
