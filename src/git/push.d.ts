export declare const gitPush: (cliDir: string) => Promise<boolean>;
export declare const pushAll: (title: string | undefined, path: string) => Promise<void>;
