"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.pushAll = exports.gitPush = void 0;
const fs_1 = __importDefault(require("fs"));
const isomorphic_git_1 = __importDefault(require("isomorphic-git"));
const node_1 = __importDefault(require("isomorphic-git/http/node"));
const loading_1 = require("../utils/loading");
const credential_1 = require("./credential");
const path_1 = require("path");
const sync_pkg_json_1 = require("./sync-pkg-json");
exports.gitPush = (cliDir) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const globby = require("globby");
        loading_1.loading.start("Adding files...");
        const paths = yield globby(["./**", "./**/.*"], {
            gitignore: true,
            cwd: cliDir,
        });
        for (const filepath of paths) {
            const p = yield isomorphic_git_1.default.add({ fs: fs_1.default, dir: cliDir, filepath });
        }
        loading_1.loading.info("Files Added");
        loading_1.loading.start("Committing changes...");
        let sha = yield isomorphic_git_1.default.commit({
            fs: fs_1.default,
            dir: cliDir,
            author: {
                name: "rizky",
                email: "rizky@andromedia.co.id",
            },
            message: "testing commit",
        });
        loading_1.loading.info("Changes commited");
        const credential = yield credential_1.gitCredential();
        loading_1.loading.start("Pushing...");
        let pushResult = yield isomorphic_git_1.default.push({
            fs: fs_1.default,
            http: node_1.default,
            dir: cliDir,
            remote: "origin",
            force: true,
            onAuth: () => credential,
        });
        loading_1.loading.info("Git push successful");
        loading_1.loading.clear();
        return true;
    }
    catch (e) {
        loading_1.loading.clear();
        if (loading_1.loading.isRunning()) {
            loading_1.loading.stop();
        }
        if (e && e.data) {
            const data = e.data;
            if (data && data.statusCode >= 400) {
                const rootDir = path_1.join(process.cwd(), "..", "..");
                const credFile = path_1.join(rootDir, "cli.credential");
                fs_1.default.unlinkSync(credFile);
            }
            console.error("Git push failed:");
            console.error(e.data);
        }
        else {
            console.error(e);
        }
        return false;
    }
});
exports.pushAll = (title = "", path) => __awaiter(void 0, void 0, void 0, function* () {
    console.log(`\nPushing ${title}...`);
    console.log("\n# CLI Dev: ");
    yield exports.gitPush(path_1.join(path));
    yield sync_pkg_json_1.syncPackageJson();
    console.log("\n# CLI Master: ");
    yield exports.gitPush(path_1.join(path, "dist"));
});
