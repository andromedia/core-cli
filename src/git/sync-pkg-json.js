"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.syncPackageJson = void 0;
const fs_1 = __importDefault(require("fs"));
const path_1 = require("path");
exports.syncPackageJson = () => {
    const pkg = JSON.parse(fs_1.default.readFileSync("package.json", "utf-8"));
    if (pkg.devDependencies) {
        delete pkg.devDependencies;
    }
    fs_1.default.writeFileSync(path_1.join(process.cwd(), "dist", "package.json"), JSON.stringify(pkg, null, 2));
};
