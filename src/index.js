"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.pushAll = void 0;
const path_1 = require("path");
const deploy_1 = require("./deploy");
const dev_1 = require("./dev");
const push_1 = require("./git/push");
Object.defineProperty(exports, "pushAll", { enumerable: true, get: function () { return push_1.pushAll; } });
const upgrade_1 = require("./prisma/upgrade");
const mode = process.argv[2];
const root = path_1.join(process.cwd(), "..", "..");
if (process.cwd() === path_1.join(root, "libs", "cli")) {
    switch (mode) {
        case "push":
            push_1.pushAll("CLI", process.cwd());
            break;
        case "dev":
            dev_1.dev();
            break;
        case "deploy":
            deploy_1.deploy();
            break;
        case "upgrade-prisma":
            upgrade_1.upgradePrisma();
            break;
    }
}
