"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.moveDir = void 0;
const fs_extra_1 = __importDefault(require("fs-extra"));
const path_1 = __importDefault(require("path"));
let promiseAllWait = function (promises) {
    // this is the same as Promise.all(), except that it will wait for all promises to fulfill before rejecting
    let all_promises = [];
    for (let i_promise = 0; i_promise < promises.length; i_promise++) {
        all_promises.push(promises[i_promise]
            .then(function (res) {
            return { res: res };
        })
            .catch(function (err) {
            return { err: err };
        }));
    }
    return Promise.all(all_promises).then(function (results) {
        return new Promise(function (resolve, reject) {
            let is_failure = false;
            let i_result;
            for (i_result = 0; i_result < results.length; i_result++) {
                if (results[i_result].err) {
                    is_failure = true;
                    break;
                }
                else {
                    results[i_result] = results[i_result].res;
                }
            }
            if (is_failure) {
                reject(results[i_result].err);
            }
            else {
                resolve(results);
            }
        });
    });
};
let movePromiser = function (from, to, records) {
    return fs_extra_1.default.move(from, to).then(function () {
        records.push({ from: from, to: to });
    });
};
exports.moveDir = function (from_dir, to_dir) {
    return fs_extra_1.default.readdir(from_dir).then(function (children) {
        return fs_extra_1.default
            .ensureDir(to_dir)
            .then(function () {
            let move_promises = [];
            let moved_records = [];
            let child;
            for (let i_child = 0; i_child < children.length; i_child++) {
                child = children[i_child];
                move_promises.push(movePromiser(path_1.default.join(from_dir, child), path_1.default.join(to_dir, child), moved_records));
            }
            return promiseAllWait(move_promises).catch(function (err) {
                let undo_move_promises = [];
                for (let i_moved_record = 0; i_moved_record < moved_records.length; i_moved_record++) {
                    undo_move_promises.push(fs_extra_1.default.move(moved_records[i_moved_record].to, moved_records[i_moved_record].from));
                }
                return promiseAllWait(undo_move_promises).then(function () {
                    throw err;
                });
            });
        })
            .then(function () {
            return fs_extra_1.default.rmdir(from_dir);
        });
    });
};
