"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.upgradePrisma = void 0;
const axios_1 = __importDefault(require("axios"));
const download_package_tarball_1 = __importDefault(require("download-package-tarball"));
const fs_1 = require("fs");
const fs_extra_1 = __importStar(require("fs-extra"));
const path_1 = require("path");
const rimraf_promise_1 = __importDefault(require("rimraf-promise"));
const loading_1 = require("../utils/loading");
const replace_between_1 = require("../utils/replace-between");
exports.upgradePrisma = () => __awaiter(void 0, void 0, void 0, function* () {
    const root = path_1.join(process.cwd(), "..", "..", "app");
    const prisma = path_1.join(root, "prisma");
    const newdir = path_1.join(prisma, "client-prisma-new");
    const current = path_1.join(prisma, "client-prisma");
    const tempdir = path_1.join(prisma, "client-prisma-temp");
    if (fs_1.existsSync(tempdir)) {
        yield rimraf_promise_1.default(tempdir);
    }
    if (fs_1.existsSync(newdir)) {
        yield rimraf_promise_1.default(newdir);
    }
    loading_1.loading.start("Checking @prisma/client latest version...");
    let res = yield axios_1.default.get("http://registry.npmjs.org/-/package/%40prisma%2Fclient/dist-tags");
    // const latest = res.data.dev;
    const latest = "2.10.1";
    loading_1.loading.info("Latest version: " + latest);
    loading_1.loading.start("Fetching download link...");
    res = yield axios_1.default.get(`https://registry.npmjs.org/%40prisma%2Fclient/${latest}`);
    const downloadLink = res.data.dist.tarball;
    loading_1.loading.info("Download Link: " + downloadLink);
    loading_1.loading.start("Downloading...");
    yield download_package_tarball_1.default({
        url: downloadLink,
        dir: tempdir,
    });
    yield fs_extra_1.default.copy(path_1.join(tempdir, "@prisma", "client"), newdir);
    yield rimraf_promise_1.default(tempdir);
    loading_1.loading.info("Download success");
    loading_1.loading.start("Patching...");
    yield patchPackageJson(newdir);
    yield patchGeneratorBuild(newdir);
    yield patchIndex(newdir);
    if (fs_1.existsSync(current)) {
        yield rimraf_promise_1.default(current);
    }
    yield fs_extra_1.default.copy(newdir, current);
    if (fs_1.existsSync(newdir)) {
        yield rimraf_promise_1.default(newdir);
    }
    loading_1.loading.info("Done");
});
const patchPackageJson = (newdir) => __awaiter(void 0, void 0, void 0, function* () {
    const pkg = JSON.parse(yield fs_extra_1.readFile(path_1.join(newdir, "package.json"), "utf-8"));
    delete pkg.scripts.postinstall;
    pkg.dependencies["@prisma/generated"] = "workspace:*";
    yield fs_extra_1.writeFile(path_1.join(newdir, "package.json"), JSON.stringify(pkg, null, 2));
});
const patchGeneratorBuild = (newdir) => __awaiter(void 0, void 0, void 0, function* () {
    yield replace_between_1.replaceFileBetween(path_1.join(newdir, "generator-build", "index.js"), "config.dirname", "= __dirname", `config.dirname = path.join(process.cwd(), "prisma", "client-generated");`, true);
    yield replace_between_1.replaceFileBetween(path_1.join(newdir, "generator-build", "index.js"), `name: ".prisma/client"`, `name: ".prisma/client",`, `name: "@prisma/generated",`, true);
});
const patchIndex = (newdir) => __awaiter(void 0, void 0, void 0, function* () {
    yield replace_between_1.replaceFileBetween(path_1.join(newdir, "", "index.d.ts"), "export * from '.prisma/client'", "export * from '.prisma/client'", `export * from '@prisma/generated'`, true);
    yield replace_between_1.replaceFileBetween(path_1.join(newdir, "", "index.js"), "const prisma = require('.prisma/client')", "const prisma = require('.prisma/client')", `const prisma = require('@prisma/generated')`, true);
});
