interface ILoading {
    clear: () => void;
    fail: (text?: string) => void;
    success: (text?: string) => void;
    info: (text?: string) => void;
    start: (text?: string) => void;
    stop: (text?: string) => void;
    warn: (text?: string) => void;
    isRunning: () => boolean;
}
export declare const loading: ILoading;
export {};
