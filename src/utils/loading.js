"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.loading = void 0;
const cli_spinners_1 = __importDefault(require("cli-spinners"));
const owy = require("owy");
const dots = cli_spinners_1.default.triangle;
const _spin = new owy.Spinner("Shining", {
    style: {
        interval: dots.interval,
        stages: dots.frames,
    },
});
_spin.isRunning = () => {
    if (_spin.spin)
        return true;
    return false;
};
exports.loading = _spin;
