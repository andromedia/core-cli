export declare const replaceBetween: (text: string, start: string, end: string, replace: string, whole?: boolean) => string;
export declare const replaceFileBetween: (file: string, start: string, end: string, replace: string, whole?: boolean) => void;
