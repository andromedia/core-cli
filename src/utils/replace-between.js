"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.replaceFileBetween = exports.replaceBetween = void 0;
const fs_1 = __importDefault(require("fs"));
exports.replaceBetween = (text, start, end, replace, whole = false) => {
    var iSelect = text.indexOf(start);
    var selLen = start.length;
    var iFrom = text.indexOf(end);
    if (iSelect >= 0 && iFrom >= 0) {
        if (whole) {
            text = text.replace(text.substring(iSelect, iFrom + end.length), replace);
        }
        else {
            text = text.replace(text.substring(iSelect + selLen, iFrom), replace);
        }
    }
    return text;
};
exports.replaceFileBetween = (file, start, end, replace, whole = false) => {
    const f = fs_1.default.readFileSync(file, "utf-8");
    const newf = exports.replaceBetween(f.toString(), start, end, replace, whole);
    fs_1.default.writeFileSync(file, newf);
};
